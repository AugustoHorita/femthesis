# README #

## Português brasileiro ##

Classe em LaTeX para confecção de teses de mestrado ou doutorado, de acordo com as normas da Faculdade de Engenharia Mecânica da Universidade Estadual de Campinas.

A classe provida (femthesis.cls) possui todos os comandos para geração das seções frontais e traseiras automaticamente, como capa, contra-capa, folha de assinatura etc. No momento a classe tem duas opcões adicionais para modificara geração automática: englishthesis e doutorado (phd); cuja função é mudar a geração automática de conteúdo. Um exemplo de documento principal está 
disponível como example.tex, até que a documentação seja feita.

### Aviso Legal ###

Este repositório é uma cópia de outro repositório privado de qual o autor atual faz parte. Como o administrador do repositório anterior planejava deixar o projeto público, porém não concretizou seu objetivo, esta é a publicação da classe terminada pelo autor atual.

## English ##

LaTeX class for typesetting master's and doctor's thesis, according to the rules of Faculty of Mechanical Engineering of Univesity of Campinas.

The provided class (femthesis.cls) contains all commands to automatic generation of front and back matter such as titlepage, signatures page etc. At the moment, the class has two additional options to modify its generation: englishthesis and phd (doutorado); which change how automatic generation from meta data is done accordngly. An example document is available as exmaple.tex, until any actual documentation is done.

### Disclaimer ###

This repository is a copy of another private repository which the current autor is part of. As the admin of the first repository planned to make it public but didn't pursue the project further, this is the publication of the finished class by the current author.

## What is this repository for? ##

* This repository is aimed at maintaining a comprehensive and updated template suitable for students of FEM.
* Version 1.0-rc1