%---------------------------------------- 
% created by Rodolfo Jordao in 24/01/2017,
% based on a given thesis template made by Marcos Mendes, FEM, 2016
% in order to remove bloat and modernize packages
%---------------------------------------- 

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{femthesis}[01/02/2017 Unicamp FEM Thesis Class]

%---------------------------------------- 
% based on the report class, for wider availability
%---------------------------------------- 

\LoadClass[twoside, openright]{report}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\DeclareOption{englishthesis}{\def\isEnglish{1}}
\DeclareOption{phd}{\def\PhD{1}}
\DeclareOption{doutorado}{\def\PhD{1}}
\ProcessOptions\relax

%---------------------------------------- 
% required packages for fonts
% remember to consider engines as luatex 
%---------------------------------------- 

\RequirePackage{ifluatex}
\ifluatex
	\RequirePackage{fontspec}
\else
	\RequirePackage[utf8]{inputenc}
\fi

%---------------------------------------- 
% required packages for image manipulation
%---------------------------------------- 

\RequirePackage{graphicx}

%---------------------------------------- 
%	better PDF support
%---------------------------------------- 

\RequirePackage[
	colorlinks
]{hyperref}

%---------------------------------------- 
% localization and language
%---------------------------------------- 

\RequirePackage[brazil, english]{babel}
\RequirePackage{csquotes}

%---------------------------------------- 
% sane formatting, keeping to ABNT demands
%---------------------------------------- 

\RequirePackage[top=2cm, bottom=3cm, inner=3cm, outer=2cm]{geometry}
\RequirePackage{fancyhdr}
\fancypagestyle{plain}{
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt}
	\renewcommand{\footrulewidth}{0pt}
}
\fancypagestyle{main}{
	\fancyhf{}
	\fancyhf[HRO]{\thepage}
	\fancyhf[HLE]{\thepage}
	\fancyhf[FRO]{\Author}
	\fancyhf[FLE]{\Author}
	\fancyhf[HC]{\@currentlabelname}
	\renewcommand{\headrulewidth}{0.3pt}
	\renewcommand{\footrulewidth}{0.3pt}
}
\RequirePackage[explicit]{titlesec}
\titleformat{\chapter}
	{\huge\bfseries}{\thechapter}{1ex}{\MakeUppercase{#1}}
\titleformat{\section}
	{\Large\bfseries}{\thesection}{1ex}{\MakeUppercase{#1}}
\titleformat{\subsection}
	{\large\bfseries}{\thesubsection}{1ex}{#1}

%---------------------------------------- 
% packages to help generation
%---------------------------------------- 

\RequirePackage{datetime}

%---------------------------------------- 
% packages for TOC
%---------------------------------------- 

\RequirePackage{glossaries}
\RequirePackage{tocloft}
\tocloftpagestyle{empty}

%---------------------------------------- 
% metadata auxiliary functions
%---------------------------------------- 

\renewcommand{\author}[1]{
	\def\Author{#1}
	\def\Autor{#1}
}
\newcommand{\autor}[1]{
	\author{#1}
}
\renewcommand{\title}[1]{
	\def\Title{#1}
}
\newcommand{\titulo}[1]{
	\def\Titulo{#1}
}
\newcommand{\advisor}[1]{
	\def\Advisor{#1}
	\def\Orientador{#1}
}
\newcommand{\orientador}[1]{
	\advisor{#1}
}
\newcommand{\coadvisor}[1]{
	\def\Coadvisor{#1}
	\def\Coorientador{#1}
}
\newcommand{\coorientador}[1]{
	\coadvisor{#1}
}
\newcommand{\dept}[1]{
	\def\Dept{#1}
}
\newcommand{\type}[1]{
	\def\Type{#1}
}

\newcommand{\printauthor}{
		\ifdefined\isEnglish
			\ifdefined\Author
				\Author
			\else
				\ifdefined\Autor
					\Autor
				\else
					Author Not defined
				\fi
			\fi
		\else
			\ifdefined\Author
				\Author
			\else
				\ifdefined\Autor
					\Autor
				\else
					 Autor Não Definido
				\fi
			\fi
		\fi
}

\newcommand\printautor{
	\printauthor
}

\newcommand{\printauthorpretty}{
	\begin{center}
		{\bf \large \printauthor}
	\end{center}
}

\newcommand{\printadvisor}{
	\ifdefined\isEnglish
		\ifdefined\Advisor
			\Advisor
		\else
			\ifdefined\Orientador
				\Orientador
			\else
				Advisor Not defined
			\fi
		\fi
	\else
		\ifdefined\Advisor
			\Advisor
		\else
			\ifdefined\Orientador
				\Orientador
			\else
				Orientador Não Definido
			\fi
		\fi
	\fi
}

\newcommand\printorientador{
	\printadvisor
}

\newcommand{\printadvisorpretty}{
	\printadvisor
}

\newcommand{\printcoadvisor}{
	\ifdefined\isEnglish
		\ifdefined\Coadvisor
			\Coadvisor
		\else
			\ifdefined\Coorientador
				\Coorientador
			\else
				Coadvisor Not defined
			\fi
		\fi
	\else
		\ifdefined\Coadvisor
			\Coadvisor
		\else
			\ifdefined\Coorientador
				\Coorientador
			\else
				Coorientador Não Definido
			\fi
		\fi
	\fi
}

\newcommand\printcoorientador{
	\printcoadvisor
}

\newcommand{\printtitle}{
	\begin{center}
		\ifdefined\Title
			\linespread{1.8}
			\selectfont
			{\bf \huge \Title}
		\else
			{\em Title Not Defined}
		\fi
	\end{center}
}

\newcommand{\printtitulo}{
	\begin{center}
		\ifdefined\Titulo
			\linespread{1.8}
			\selectfont
			{\bf \huge \Titulo}
		\else
			{\em Título Não definido}
		\fi
	\end{center}
}

%---------------------------------------- 
% make commands for automatic pages generation
%---------------------------------------- 

\newcommand{\makefrontmatter}{
	%\pagenumbering{gobble}
	\pagestyle{plain}
}

\newcommand{\makemainmatter}{
	%\pagenumbering{arabic}
	\pagestyle{main}
}

\newcommand{\maketitlepage}{
	\begin{minipage}{0.1\textwidth}
		\centering
		\includegraphics[width=\textwidth]{unicamp}
	\end{minipage}
	\begin{minipage}{0.8\textwidth}
		\centering
		\linespread{1.5}
		\selectfont
		{\Large \bf UNIVERSIDADE ESTADUAL DE CAMPINAS \newline FACULDADE DE ENGENHARIA MECÂNICA}
	\end{minipage}
	\begin{minipage}{0.1\textwidth} 
		\centering
	\end{minipage}

	\vfill

	\printauthorpretty
	
	\vfill 

	\ifdefined\isEnglish
		\printtitle

		\vfill
	\fi

	\printtitulo

	\vfill

	\begin{center}
		CAMPINAS \\ \the\year
	\end{center}

	\cleardoublepage
}

\newcommand{\makesignaturepage}[2]{

	\def\Preambulo{#1}
	\def\Preamble{#2}

	\printauthorpretty

	\vfill

	\printtitle

	\vfill

	\printtitulo

	\vfill

		\ifdefined\isEnglish
				\begin{minipage}{0.49\textwidth}
					\hfill
				\end{minipage}
				\begin{minipage}{0.49\textwidth}
					\ifx\Preamble\empty
						{\em Preamble not defined}
					\else
						\Preamble
					\fi
				\end{minipage}

				\vfill
		\fi

		\begin{minipage}{0.49\textwidth}
			\hfill
		\end{minipage}
		\begin{minipage}{0.49\textwidth}
			\ifx\Preambulo\empty
				{\em Preâmbulo não definido}
			\else
				\Preambulo
			\fi
		\end{minipage}

	\vfill

	\noindent \ifdefined\isEnglish Advisor: \else Orientador: \fi \printadvisor \\
	\ifdefined\isEnglish Coadvisor: \else Coorientador \fi \printcoadvisor

	\vfill

	\noindent
	\begin{minipage}{0.59\textwidth}
		ESTE EXEMPLAR CORRESPONDE À VERSÃO FINAL DA \ifdefined\Phd TESE \else DISSERTAÇÃO \fi DEFENDIDA PELO ALUNO\MakeUppercase\printauthor, E ORIENTADO PELO\MakeUppercase\printadvisor.
	\end{minipage}
	\begin{minipage}{0.4\textwidth}
		\hfill
	\end{minipage}

	\vfill

	\noindent
	\rule{0.6\textwidth}{1pt}\\
	ASSINATURA DO ORIENTADOR

	\vfill

	\begin{center}
		CAMPINAS \\ \the\year
	\end{center}

	\cleardoublepage
	
}

\newcommand{\makecatalogpage}[1][]{
	\def\Catalog{#1}
	\ifx\Catalog\empty
		\begin{center}
			{\large FICHA CATALOGRÁFIA ELABORADA PELA \\ BIBLIOTECA DE ÁREA DE ENGENHARIA E ARQUITETURA - BAE - UNICAMP}
		\end{center}

		\vfill

		{\Huge A SER PREENCHIDO POR PDF DA BAE}

		\vfill

		{\Huge TO BE FILLED WITH A PDF FROM BAE}

		\vfill
	\else
		\includegraphics{\Catalog}
	\fi

	\cleardoublepage
}

\newcommand{\makeapprovalpage}{
	\begin{center}
		\linespread{1.5}
		\selectfont
		{\large UNIVERSIDADE ESTADUAL DE CAMPINAS\\
		FACULDADE DE ENGENHARIA MECÂNICA\\
		COMISSÃO DE PÓS-GRADUAÇÃO EM ENGENHARIA MECÂNICA\\
		\ifdefined\Dept\MakeUppercase\Dept\else Dept Undefined\fi}
	\end{center}

	\vfill

	\begin{center}
		{\bf \ifdefined\PhD TESE DE DOUTORADO\else TESE DE MESTRADO\fi}
		%{\bf \ifdefined\Type\MakeUppercase\Type\else Tipo undefined\fi}
	\end{center}

	\vfill

	\ifdefined\isEnglish
		\printtitle

		\vfill
	\fi

	\printtitulo

	\vfill

	\noindent \ifdefined\isEnglish Author:\else Autor:\fi \printauthor \\
	\noindent \ifdefined\isEnglish Advisor:\else Advisor:\fi \printadvisor \\
	\noindent \ifdefined\isEnglish Coadvisor:\else Coadvisor:\fi \printcoadvisor

	\vfill

	\noindent A banca examinadora composta pelos membros abaixo aprovou esta tese:
	
	\vspace{3ex}

	\noindent \rule{0.8\textwidth}{1pt} \\
	Prof. Dr. \\
	Instituição

	\vspace{1.5ex}

	\noindent \rule{0.8\textwidth}{1pt} \\
	Prof. Dr. \\
	Instituição

	\vspace{1.5ex}

	\noindent \rule{0.8\textwidth}{1pt} \\
	Prof. Dr. \\
	Instituição

  \ifdefined\Phd

	\vspace{1.5ex}

	\noindent \rule{0.8\textwidth}{1pt} \\
	Prof. Dr. \\
	Instituição

	\vspace{1.5ex}

	\noindent \rule{0.8\textwidth}{1pt} \\
	Prof. Dr. \\
	Instituição

  \fi

	\vfill

	\ifdefined\isEnglish\selectlanguage{brazil}\fi
	Campinas, \today%\the\day de \monthname  de \the\year
	\ifdefined\isEnglish\selectlanguage{english}\fi

	\cleardoublepage

}

\newcommand{\makededicatorypage}[1]{
	\hfill
	\vfill

	\begin{center}
		{\large \bf \ifdefined\isEnglish Dedicatory\else Dedicatória\fi}
	\end{center}

	\vspace{2ex}

	\begin{center}
		#1
	\end{center}

	\vfill

	\cleardoublepage
}

\newcommand{\makethankspage}[1]{
	\hfill
	\vfill

	\begin{center}
		{\bf \large \ifdefined\isEnglish Thanks\else Agradecimentos\fi}
	\end{center}

	\vspace{2ex}

	\begin{center}
		#1
	\end{center}

	\vfill

	\cleardoublepage
}

\newcommand{\makeabstractpage}[2]{
	\hfill
	\vfill

	\begin{center}
		{\bf \Large Abstract}
	\end{center}

	\vspace{2ex}

	\begin{center}
		#1
	\end{center}

	\vspace{3ex}

	{\em Keywords}: #2

	\vfill

	\cleardoublepage
}

\newcommand{\makeresumopage}[2]{
	\hfill
	\vfill

	\begin{center}
		{\bf \Large Resumo}
	\end{center}

	\vspace{2ex}

	\begin{center}
		#1
	\end{center}

	\vspace{3ex}

	{\em Palavras-chave}: #2

	\vfill

	\cleardoublepage
}

\newcommand{\makeabstractresumopage}[4]{
	\hfill
	\vfill

	\begin{center}
		{\bf \Large Abstract}
	\end{center}

	\vspace{2ex}

	\begin{center}
		#1
	\end{center}

	\vspace{3ex}

	{\em Keywords}: #2

	\vfill

	\begin{center}
		{\bf \Large Resumo}
	\end{center}

	\vspace{2ex}

	\begin{center}
		#3
	\end{center}

	\vspace{3ex}

	{\em Palavras-chave}: #4

	\vfill

	\cleardoublepage

}

\newcommand{\makelistoffigures}{
	\listoffigures
	\cleardoublepage
	%\pagestyle{plain}
}

\newcommand{\makelistoftables}{
	\listoftables
	\cleardoublepage
	%\pagestyle{plain}
}

\newcommand{\makeglossarypage}{
	\printglossaries
	\cleardoublepage
	%\pagestyle{plain}
}

\newcommand{\maketableofcontents}{
	\tableofcontents
	\cleardoublepage
	%\pagestyle{plain}
}
